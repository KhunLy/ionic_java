import { Component } from '@angular/core';
import { AlertController, ActionSheetController } from '@ionic/angular';
import { Article } from '../_models/article';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {


  newArticle: string;
  articles: Article[];

  constructor(
    private alertController: AlertController,
    private asCtrl: ActionSheetController,
    private storage: Storage
  ) {
    this.articles = [];
    this.storage.get("DATA").then(data => {
      this.articles = data || []
    });
  }

  add() {
    if(this.newArticle) {
      this.articles.push({
        name: this.newArticle,
        isChecked: false
      });
      this.newArticle = null;
      this.save();
    }
  }

  async clear() {
    // this.alertController.create({
    //   header: "Attention",
    //   message: "Êtes -vous sûr de vouloir tout supprimer"
    // }).then((alert) => {
    //   alert.present()
    // });
    let alert = await this.alertController.create({
      header: "Attention",
      message: "Êtes -vous sûr de vouloir tout supprimer?",
      buttons: [
        { 
          text: "non"
        },{
          text: "oui",
          handler: () => {
            this.articles = [];
            this.save();
          } 
        }
      ]
    });
    alert.present(); 
  }

  async presentActionSheet(article: Article) {
    let as = await this.asCtrl.create({
      header: "Actions",
      buttons: [
        {
          text: article.isChecked ? "UnCheck" : "Check",
          handler: () => {
            article.isChecked = !article.isChecked;
            this.save();
          }
        },{
          text: "Delete",
          role: "destructive",
          handler: () => {
            if(!article.isChecked) return;
            this.articles = this.articles.filter((item) => item != article);
            this.save();
          }
        },{
          text: "Cancel",
          role: "cancel"
        }
      ]
    });
    as.present();
  }

  save () {
    this.storage.set("DATA", this.articles);
  }

}
